﻿using System;

[AttributeUsage(AttributeTargets.Field)]
public class PrefabPathAttribute: System.Attribute {
	public readonly string path;

	public PrefabPathAttribute(string path) {
		this.path = path;
	}
}
