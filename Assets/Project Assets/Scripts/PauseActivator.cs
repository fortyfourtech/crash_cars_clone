﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseActivator : MonoBehaviour {
	UserControl m_playerInput;
//	GameObject m_levelHUD;

	void Awake() {
		var character = GameObject.FindWithTag ("Player");
//		m_playerInput = character.GetComponent <UserControl> ();
//		m_levelHUD = character.transform.Find ("HUD").gameObject;
	}

	public void ActivatePause() {
		AudioListener.pause = true;
		m_playerInput.enabled = false;
//		m_levelHUD.SetActive (false);
		Time.timeScale = 0;
	}

	public void DeactivatePause() {
		AudioListener.pause = false;
		m_playerInput.enabled = true;
//		m_levelHUD.SetActive (true);
		Time.timeScale = 1;
	}
}
