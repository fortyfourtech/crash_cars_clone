﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawnPoint: MonoBehaviour {
	// in seconds
	public float spawnDelay;
	[SerializeField] GameObject prefabToSpawn;
	Transform spawnedObject;

	void Start () {
		StartCoroutine (SpawnPickupCoroutine ());
	}
	
	IEnumerator SpawnPickupCoroutine () {
		while (true) {
			// spawn new one
			spawnPrefab ();
			// while spawned object is alive dont do anything
			while (spawnedObject != null) {
				yield return null;
			}
			// after it was destroyed wait for delay before spawn new one
			yield return new WaitForSeconds (spawnDelay);
		}
	}

	void spawnPrefab() {
		var spawned = Instantiate (prefabToSpawn, transform.position, transform.rotation);
		spawnedObject = spawned.transform;
	}
}
