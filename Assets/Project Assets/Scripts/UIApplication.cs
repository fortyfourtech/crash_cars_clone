﻿using UnityEngine;

public class UIApplication {

	static public void setupArrowCursor() {
		Cursor.visible = true;
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);

//		var oldCursor = GameObject.FindGameObjectWithTag ("Cursor");
//		if (oldCursor != null) {
//			GameObject.Destroy (oldCursor);
//		}
	}

	static public void setupGameCursor() {
		Cursor.visible = false;

		var oldCursor = GameObject.FindGameObjectWithTag ("Cursor");
		if (oldCursor != null) {
			GameObject.Destroy (oldCursor);
		}

		var cursorPrefab = ResourceProvider.getPrefab (UIPrefab.playerCursor);
		var cursor = GameObject.Instantiate (cursorPrefab);
		cursor.tag = "Cursor";
	}

	static public void setupNoCursor() {
		Cursor.visible = false;
	}
}
