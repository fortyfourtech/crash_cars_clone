﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class Bullet : MonoBehaviour {

	public float initialSpeed = 1;
	public Vector3 initialDirection;
	public float damage;
	public GameObject sender;

	Rigidbody m_rigidbody;
	TrailRenderer m_trailRenderer;
	bool flying = true;

	float kBulletForce = 10;

	void Awake() {
		m_rigidbody = GetComponent<Rigidbody> ();
		m_trailRenderer = GetComponent<TrailRenderer> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (flying)
			m_rigidbody.velocity = initialDirection*initialSpeed;
	}

	void OnCollisionEnter(Collision collision) {
		if (flying) {
			var other = collision.collider;
			IHittable otherScript = other.GetComponentInParent <IHittable> ();
			if (otherScript != null && other.gameObject != sender) {
				otherScript.receiveWeaponHit (damage);
			}

			bool isJustCollider = other.isTrigger;
//		MeshCollider otherMeshCollider = other.GetComponentInChildren<MeshCollider> ();
//		if (otherMeshCollider != null)
//			isJustCollider = false;
//		Collider[] otherColliders = other.GetComponentsInChildren<Collider> ();
//		if (otherColliders != null)
//			for (int i = 0; i < otherColliders.Length && isJustCollider; i++)
//				if (!otherColliders [i].isTrigger)
//					isJustCollider = false;
			var isAnotherBullet = other.GetComponent<Bullet> () != null;
			if (!isJustCollider && !isAnotherBullet) {
				flying = false;
				// destroy bullet immediately
				Destroy (gameObject);
			}
		}
	}
}
