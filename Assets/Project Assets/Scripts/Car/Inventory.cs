﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CoinsUpdateEvent(int n);

public class Inventory: MonoBehaviour {
	public event CoinsUpdateEvent coinsNumberChanged;
	public int coinsNumber = 0;

	public bool addCoin() {
//		if (coinsNumber < maxMineNumber) {
		coinsNumber++;
		if (coinsNumberChanged != null)
			coinsNumberChanged (coinsNumber);
		return true;
//		} else {
//			return false;
//		}

	}

	public bool consumeCoins(int number) {
		if (coinsNumber > 0) {
			coinsNumber-= number;
			if (coinsNumberChanged != null)
				coinsNumberChanged (coinsNumber);
			return true;
		} else {
			return false;
		}
	}
}
