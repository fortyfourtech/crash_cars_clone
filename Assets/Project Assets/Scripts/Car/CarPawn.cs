﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPawn : MonoBehaviour, IHittable {
	public delegate void CarWeaponEvent(GameObject w);
	public event CarWeaponEvent weaponPicked;
	public event CarWeaponEvent weaponOver;
	public event HealthEvent dyingEnded;

	[SerializeField] private int coinsToDrop = 2;

	[SerializeField] private float m_maxSpeed = 10;
	public bool hasWeapon { get { return m_weapon != null; } }
	public bool isHorizontal { get {
			var xRot = Mathf.Abs (m_rigidbody.rotation.eulerAngles.x);
			var zRot = Mathf.Abs (m_rigidbody.rotation.eulerAngles.z);
			return (xRot < 10 || xRot > 350) && (zRot < 10 || zRot > 350);
		} }
	public bool fullyLanded { get { return groundedWheelsNumber == 4 && isHorizontal; } }
	public float healthPercents { get { return 100f * m_health.healthPoints / m_health.maxHealth; } }

	[SerializeField] private float m_mass = 5;
	[SerializeField] private Transform cemterOfMass;
	[SerializeField] private WheelCollider[] m_WheelColliders = new WheelCollider[4];
	[SerializeField] private Transform m_weaponContainer;
	private Rigidbody m_rigidbody;
	private IShootable m_weapon;
	private Health m_health;
	public Inventory m_inventory;
	private int groundedWheelsNumber = 0;
	private bool moveForward = true;

	public static float k_speedMultiplier = 2f;
	public float k_accelerationForce = 2f; // TODO remove public modifier
	public static float k_massMultiplier = 2f;
	public float k_steerForce = 2f; // TODO remove public modifier
	float k_maximumMass = 10;
	public float k_hurtTime = 0.5f; // TODO remove public modifier
	float k_maximumAngle = 45f;

	void Awake() {
		m_rigidbody = GetComponent<Rigidbody> ();
		m_health = GetComponent<Health> ();

	}

	void Start () {
		setMass (m_mass);
		m_rigidbody.centerOfMass = cemterOfMass.localPosition;

		var weaponAttached = m_weaponContainer.GetComponentInChildren<IShootable> ();
		if (weaponAttached != null)
			m_weapon = weaponAttached;

		gameObject.AddComponent<CarCollider> ();
		m_inventory = gameObject.AddComponent<Inventory> ();
	}

	void Update () {
	}

	void FixedUpdate() {
		groundedWheelsNumber = 0;
		foreach (var wheel in m_WheelColliders) {
			var wheelHit = new WheelHit ();
			if (wheel.GetGroundHit (out wheelHit)) {
				var collider = wheelHit.collider;
				var normalAngle = Vector3.Angle (Vector3.up, wheelHit.normal);
				if (normalAngle <= k_maximumAngle)
					groundedWheelsNumber += 1;
			}
		}

		var maxSpeed = m_maxSpeed * k_speedMultiplier;
		if (moveForward && m_rigidbody.velocity.magnitude < maxSpeed && groundedWheelsNumber > 0) // add velocity
			m_rigidbody.AddForce (transform.forward*k_accelerationForce*10f * (moveForward ? 1 : -1), ForceMode.Impulse);
//		if (groundedWheelsNumber == 4) { // add force that pushes down
//			m_rigidbody.AddForce (-transform.up * k_accelerationForce*2f, ForceMode.Impulse);
//		}

		// this rotates the car to horizontal state when it is in the air
		if (groundedWheelsNumber == 0) {
			m_rigidbody.rotation = Quaternion.Lerp (m_rigidbody.rotation, Quaternion.Euler (0, m_rigidbody.rotation.eulerAngles.y, 0), Time.fixedDeltaTime*10f);
		}
	}

	// ---------------------------------
	// changing physical characteristics

	public void setMass (float newMass) {
		newMass = Mathf.Clamp(newMass, 0, k_maximumMass);
		m_mass = newMass;
		m_rigidbody.mass = m_mass * k_massMultiplier;
	}

	public void setMaxSpeed (float newSpeed) {
//		newSpeed = Mathf.Clamp(newSpeed, 0, k_maximumMass);
		m_maxSpeed = newSpeed;
	}

	// ---------------------------------
	// controlling

	// turn, break
	public void move (float steering, bool slowDown = false) {
		moveForward = !slowDown;

		steering = Mathf.Clamp(steering, -1, 1);
		steering *= k_steerForce;
		transform.Rotate (0, steering, 0, Space.Self);
		if (fullyLanded)
			m_rigidbody.velocity = Quaternion.Euler (0, steering, 0) * transform.forward * m_rigidbody.velocity.magnitude;
	}

	public void startShooting() {
		if (m_weapon != null)
			m_weapon.startShooting ();
	}

	public void stopShooting() {
		if (m_weapon != null)
			m_weapon.stopShooting ();
	}

	// ---------------------------------
	// interactions with other objects in the world

	public bool pickupShootable (GameObject pickupPrefab) {
		var shootableComponent = pickupPrefab.GetComponent<IShootable> ();
		if (shootableComponent != null && !hasWeapon) {
			var pickup = Instantiate (pickupPrefab, m_weaponContainer, false);
			shootableComponent = pickup.GetComponent<IShootable> ();
			m_weapon = shootableComponent;
			// subscribe to weapon empty event
			m_weapon.clipEmpty += weaponEnded;
			// notify observers abour pickup
			if (weaponPicked != null)
				weaponPicked (pickup);
			
			return true;
		} else {
			return false;
		}
	}

	private void weaponEnded (GameObject weapon) {
		if (weaponOver != null)
			weaponOver (weapon);
		
		Destroy (weapon);
		m_weapon = null;
	}

	public void pickupCoin () {
		m_inventory.addCoin ();
	}

	public int coinNumber() {
		return m_inventory.coinsNumber;
	}

	// Health managing
	public bool heal (float healValue) {
		return m_health.heal (healValue);
	}

	private bool hurting = false;
	public void receiveBumpHit(float damageValue) {
		if (!hurting) {
			StartCoroutine (hurt());
			if (m_health.hit (damageValue))
				dying ();
		}
	}

	public void receiveWeaponHit(float damageValue) {
		if (m_health.hit (damageValue))
			dying ();
	}

	private IEnumerator hurt() {
		hurting = true;
		yield return new WaitForSeconds (k_hurtTime);
		hurting = false;
	}

	private bool pendingKill = false;
	private void dying () {
		if (pendingKill)
			return;
		else
			pendingKill = true;
		
		Destroy (gameObject);

		if (dyingEnded != null)
			dyingEnded (gameObject);

		// spawn coins
		var coinPrefab = ResourceProvider.getPrefab (PickupPrefab.coin);
		var explPosition = transform.position - new Vector3(0,0.5f,0);// + UnityEngine.Random.insideUnitSphere * 0.5f;
		var spawnBase = transform.position + new Vector3 (0, 0.5f, 0);
		for (int i = 0; i < coinsToDrop; i++) {
			var randOffset2d = Random.insideUnitCircle;
			var randOffset = new Vector3 (randOffset2d.x, 0, randOffset2d.y);
			var coinObj = Instantiate (coinPrefab,spawnBase+randOffset,Quaternion.identity);
			var coinRigidbody = coinObj.GetComponent<Rigidbody> ();
			coinRigidbody.AddExplosionForce (2000, explPosition, 3);
		}

		// spawn crap
//		var carCrap = ResourceProvider.getPrefab (ObjectPrefab.carScrap);
//		var details = carCrap.GetComponentsInChildren<Detachable> ();
//		var explPos = transform.position + UnityEngine.Random.insideUnitSphere * 0.5f;
//		foreach (var detail in details) {
//			detail.transform.SetParent (null);
//			var collider = detail.gameObject.AddComponent<MeshCollider> ();
//			collider.convex = true;
//			var rigidbody = detail.gameObject.AddComponent<Rigidbody> ();
//			rigidbody.AddExplosionForce (200, explPos, 80);
//
//			Destroy (detail.gameObject, 20+UnityEngine.Random.value*6-4);
//		}

	}
}
