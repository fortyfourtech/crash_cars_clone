﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Eppy;
using System.Linq;

using Random = UnityEngine.Random;

public class CarAI : MonoBehaviour {
	enum AIGoal {
		heal, weapon, money, kill,
		none
	}

	CarPawn m_pawn;
	[SerializeField] Transform m_target;
	[SerializeField] AIGoal m_currentGoal = AIGoal.none;

	[Space]

	[SerializeField] private float k_attackDistance = 10;
	[SerializeField] private float k_healthBoundToHeal = 30;
	[SerializeField][Range(0,3)] private float k_desireToKill = 1;
	[SerializeField] private float k_angleToStopTurn = 5;
	float k_weightHeal = 5;
	float k_weightWeapon = 4;
	float k_weightGreed = 3;
	float k_weightRage = 2;

	IEnumerator targetUpdating;

	void Awake() {
		m_pawn = GetComponent<CarPawn> ();
	}

	void Start () {
		targetUpdating = TargetUpdating ();
		StartCoroutine (targetUpdating);
	}
	
	// Update is called once per frame
	void Update () {
		if (m_currentGoal == AIGoal.none)
			return;

		if (m_target != null) {

			var path = new NavMeshPath ();
			NavMesh.CalculatePath(transform.position, m_target.position, NavMesh.AllAreas, path);
			var targetPoint = path.corners.Length > 0 ? path.corners [1] : m_target.position;
			var forwardDirection = transform.forward;
			var desiredDirection = targetPoint - transform.position;
			var turnToTarget = Vector3.Angle (desiredDirection, forwardDirection) * (Vector3.Cross (desiredDirection, forwardDirection).y < 0 ? 1 : -1);
			if (Mathf.Abs (turnToTarget) > k_angleToStopTurn) {
				m_pawn.move (1 * Mathf.Sign (turnToTarget));
			}

			var distanceToTarget = desiredDirection.magnitude;
			if (m_currentGoal == AIGoal.kill && distanceToTarget < k_attackDistance) {
				m_pawn.startShooting ();
			} else {
				m_pawn.stopShooting ();
			}
		} else { // start to update target immediately
			m_currentGoal = AIGoal.none;
			StopCoroutine (targetUpdating);
			targetUpdating = TargetUpdating ();
			StartCoroutine (targetUpdating);
		}
	}

	IEnumerator TargetUpdating() {
		while (true) {
			yield return updateTarget ();
			yield return new WaitForSeconds (Random.value + 1);
		}
	}

	IEnumerator updateTarget () {
		// calc weights for all goals
		var goalsWithWeights = new List<Tuple<AIGoal,float,Transform>> ();
		foreach (AIGoal goal in Enum.GetValues(typeof(AIGoal))) {
			var goalTarget = findGoalTarget (goal);
			float goalWeight = 0;
			if (goalTarget != null) {
				var targetDistance = (goalTarget.position - m_pawn.transform.position).magnitude;
				goalWeight = goalActuality (goal) * goalWeights [goal] / targetDistance;
			}

			goalsWithWeights.Add (Tuple.Create(goal, goalWeight, goalTarget));
//			goalsWithWeights [goal] = goalWeight;
			yield return null;
		}
		// sort weights
		goalsWithWeights.Sort ((x, y) => {
			return (int)Mathf.Sign(y.Item2-x.Item2); // to descending sort reverse items positions
		});
		// take first weight
		var properGoal = goalsWithWeights[0];
		// take goal and transform of this weight
		m_currentGoal = properGoal.Item1;
		m_target = properGoal.Item3;
	}

	Transform findGoalTarget (AIGoal goal) {
		// get all transforms that corresponds to goal
		var targets = goalTargets (goal);
		// sort transforms by distance
		Array.Sort (targets, (x, y) => {
			var distanceX = (x.position - m_pawn.transform.position).magnitude;
			var distanceY = (y.position - m_pawn.transform.position).magnitude;
			return distanceX.CompareTo (distanceY);
		});
		// take first
		var target = targets.Length > 0 ? targets[0] : null;
		return target;
	}

	float goalActuality (AIGoal goal) {
		float result;
		switch (goal) {
		case AIGoal.heal:
			if (m_pawn.healthPercents < k_healthBoundToHeal) {
				var healPriority = ((int)(k_healthBoundToHeal - m_pawn.healthPercents + 1) / (int)(k_healthBoundToHeal / 3f)) + 1;
				result = 1 * healPriority; 
			}
			else
				result = 0;
			break;
		case AIGoal.weapon:
			if (m_pawn.hasWeapon)
				result = 0;
			else
				result = 1;
			break;
		case AIGoal.kill:
			result = k_desireToKill;
			break;
		case AIGoal.money:
			result = 1;
			break;
		default:
			result = 0;
			break;
		}

		return result;
	}

	Transform[] goalTargets(AIGoal goal) {
		Type targetsType;
		switch (goal) {
		case AIGoal.heal:
			targetsType = typeof(MedkitPickup);
			break;
		case AIGoal.weapon:
			targetsType = typeof(WeaponPickup);
			break;
		case AIGoal.money:
			targetsType = typeof(CoinPickup);
			break;
		case AIGoal.kill:
			targetsType = typeof(CarPawn);
			break;
		default:
			targetsType = null;
			break;
		}

		if (targetsType == null)
			return new Transform[]{};
		else {
			var list = Transform.FindObjectsOfType (targetsType);
			var typedList = new List<Transform> ();//list.Select (x => (x as MonoBehaviour).transform); // TODO create object that will hold all spawned objects. additionally it can hold pool of objects to spawn
			Array.ForEach (list, (x) => {
				var item = x as MonoBehaviour;
				if (item.gameObject != this.gameObject)
					typedList.Add (item.transform);
			});
			return typedList.ToArray ();
		}
	}

	Dictionary<AIGoal,float> goalWeights { get { return new Dictionary<AIGoal, float> {
			{ AIGoal.heal,		k_weightHeal },
			{ AIGoal.weapon,	k_weightWeapon },
			{ AIGoal.money,		k_weightGreed },
			{ AIGoal.kill,		k_weightRage },
			{ AIGoal.none,	 	0 }
		}; } }
}
