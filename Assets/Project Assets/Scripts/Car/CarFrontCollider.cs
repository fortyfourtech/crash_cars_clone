﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarFrontCollider: MonoBehaviour {
	Rigidbody m_rigidbody;
	float selfMass;

	float k_kickForce = 1500;

	void Start() {
		m_rigidbody = GetComponentInParent<Rigidbody> ();
		selfMass = m_rigidbody.mass;
	}

	public void OnTriggerEnter (Collider collider) {
		var anotherCar = collider.GetComponentInParent<CarPawn> ();
		if (anotherCar != null) {
			print ("car forward bumped");
			var bumpDirection = transform.forward;
			collider.attachedRigidbody.AddForce (bumpDirection*k_kickForce*CarPawn.k_massMultiplier); // TODO add velocity constraint here

			// hurt him
			var velocity = m_rigidbody.velocity.magnitude / CarPawn.k_speedMultiplier;
			var mass = selfMass / CarPawn.k_massMultiplier;
			var damage = Mathf.Clamp (velocity * mass, 1f, 5f);
			anotherCar.receiveBumpHit (damage);
		}
	}
}
