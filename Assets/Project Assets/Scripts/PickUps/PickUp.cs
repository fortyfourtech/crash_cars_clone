﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class PickUp : MonoBehaviour {
	private bool m_used = false;

	protected virtual void OnTriggerEnter(Collider other) {
		var otherScript = other.GetComponentInParent<CarPawn>();
		if (otherScript != null) {
			consume (otherScript);
		}
	}

	protected void consume(CarPawn character) {
		if (!m_used && applyEffect (character)) {
			m_used = true;
			Destroy(gameObject);
		}
	}

	abstract protected bool applyEffect (CarPawn character);
}
