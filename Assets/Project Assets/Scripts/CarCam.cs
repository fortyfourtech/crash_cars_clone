using System;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace UnityStandardAssets.Cameras
{
    [ExecuteInEditMode]
    public class CarCam : PivotBasedCameraRig {
		[SerializeField] private float m_xAngle = 60;
		[SerializeField] private float m_cameraHeight = 25;
		[SerializeField] private bool initWithTransform = false;
        [SerializeField] private float m_MoveSpeed = 3; // How fast the rig will move to keep up with target's position

		protected override void Start () {
			base.Start ();

			if (initWithTransform) {
				m_cameraHeight = m_Pivot.localPosition.y;
				m_xAngle = m_Pivot.localRotation.eulerAngles.x;
			} else {
				m_Pivot.localPosition = new Vector3 (0, m_cameraHeight, 0);
				m_Pivot.localRotation = Quaternion.Euler (m_xAngle, 0, 0);
			}

			m_Pivot.localPosition = new Vector3 (m_Pivot.localPosition.x, m_Pivot.localPosition.y, -m_cameraHeight / Mathf.Tan (m_xAngle * Mathf.Deg2Rad));
		}

        protected override void FollowTarget(float deltaTime) {
            // if no target, or no time passed then we quit early, as there is nothing to do
            if (!(deltaTime > 0) || m_Target == null) {
                return;
            }

            // initialise some vars, we'll be modifying these in a moment
            var targetForward = m_Target.forward;
            var targetUp = m_Target.up;

            // camera position moves towards target position:
			var rayHit = new RaycastHit();
			float cameraY = Physics.Raycast (m_Target.position + Vector3.up * 0.1f, Vector3.down, out rayHit) ? rayHit.point.y : -1f;
			var desiredPos = new Vector3 (m_Target.position.x, cameraY, m_Target.position.z);
			transform.position = Vector3.Lerp(transform.position, desiredPos, deltaTime*m_MoveSpeed);

            // camera's rotation
			transform.rotation = Quaternion.identity;//(transform.rotation, rollRotation, m_TurnSpeed*m_CurrentTurnAmount*deltaTime);
        }
    }
}
